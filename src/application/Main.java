package application;
	
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,700,610);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			Tab keyGen = new Tab("Key Generation");
			keyGen.setClosable(false);
			Tab encrypt = new Tab("Encryption");
			encrypt.setClosable(false);
			Tab decrypt = new Tab("Decryption");
			decrypt.setClosable(false);
			TabPane tabpane = new TabPane(keyGen, encrypt, decrypt);
			
			TextField order = new TextField();
			order.setPromptText("Enter the cyclic group order");
			TextField gen = new TextField();
			gen.setPromptText("Enter the generator");
			TextField secretKey = new TextField();
			secretKey.setPromptText("Secret Key");
			TextField publicKey = new TextField();
			publicKey.setPromptText("Public Key");
			Button generate = new Button("Generate");
			
			GridPane gridpane1 = new GridPane();
			ColumnConstraints column1 = new ColumnConstraints();
		    column1.setPercentWidth(90);
		    generate.prefWidthProperty().bind(gridpane1.widthProperty().multiply(1));
		    gridpane1.getColumnConstraints().addAll(column1);
			gridpane1.add(setText("Generation of ElGamal Key Pair"), 0, 0);
			gridpane1.add(setProperty("Enter value of the prime number q (Not necesary)"), 0, 1);
			gridpane1.add(order, 0, 2);
			gridpane1.add(setProperty("Enter the value of the generator g (required)"), 0, 3);
			gridpane1.add(gen, 0, 4);
			gridpane1.add(setProperty("Enter the Secret Key (Not necesary)"), 0, 5);
			gridpane1.add(secretKey, 0, 6);
			gridpane1.add(setProperty("The Public Key is: (g^secretKey)mod(q) = "), 0, 8);
			gridpane1.add(publicKey, 0, 9);
			gridpane1.add(generate, 0, 7);
			keyGen.setContent(gridpane1);
			
			generate.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					String[] genTab = keyPairGen(order.getText(), secretKey.getText(), gen.getText());
					order.setText(genTab[0]);
					gen.setText(genTab[1]);
					secretKey.setText(genTab[2]);
					publicKey.setText(genTab[3]);
				}
			});
			
			TextField order1 = new TextField();
			order1.setPromptText("Enter the cyclic group order");
			TextField gen1 = new TextField();
			gen1.setPromptText("Enter the generator");
			TextField publicKey1 = new TextField();
			publicKey1.setPromptText("Recipient Public Key");
			TextArea message = new TextArea();
			message.setPromptText("Enter the message to encrypt");
			Button encryptMessage = new Button("Encrypt");
			TextArea encryptionResult = new TextArea();
			encryptionResult.setPromptText("See the ciphered message here !!!");
			
			GridPane gridpane2 = new GridPane();
			ColumnConstraints column11 = new ColumnConstraints();
		    column11.setPercentWidth(90);
		    encryptMessage.prefWidthProperty().bind(gridpane2.widthProperty().multiply(1));
		    gridpane2.getColumnConstraints().addAll(column11);
			gridpane2.add(setText("ElGamal Encryption"), 0, 0);
			gridpane2.add(setProperty("Enter value of the prime number q"), 0, 1);
			gridpane2.add(order1, 0, 2);
			gridpane2.add(setProperty("Enter the value of the generator"), 0, 3);
			gridpane2.add(gen1, 0, 4);
			gridpane2.add(setProperty("Enter the public Key of recipient"), 0, 5);
			gridpane2.add(publicKey1, 0, 6);
			gridpane2.add(setProperty("Enter the message you want to encrypt"), 0, 7);
			gridpane2.add(message, 0, 8);
			gridpane2.add(encryptMessage, 0, 9);
			gridpane2.add(setProperty("The encrypted data is :"), 0, 10);
			gridpane2.add(encryptionResult, 0, 11);
			encrypt.setContent(gridpane2);
			
			encryptMessage.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					BigInteger p = new BigInteger(order1.getText());
					BigInteger g = new BigInteger(gen1.getText());
					BigInteger h = new BigInteger(publicKey1.getText());
					LinkedList<BigInteger> cipherText = encrypt(p, g, h, message.getText());
					encryptionResult.setText(cipherText.toString());
				}
			});
			
			TextArea cipher = new TextArea();
			cipher.setPromptText("Enter the ciphered message");
			TextField secretKey1 = new TextField();
			secretKey1.setPromptText("Enter the secret key");
			TextField order2 = new TextField();
			order2.setPromptText("Enter the group order");
			Button decryptButton = new Button("decrypt");
			TextArea clearMessage = new TextArea();
			clearMessage.setPromptText("See the clear message here !!!");
			
			GridPane gridpane3 = new GridPane();
			ColumnConstraints column111 = new ColumnConstraints();
		    column111.setPercentWidth(90);
		    decryptButton.prefWidthProperty().bind(gridpane3.widthProperty().multiply(1));
		    gridpane3.getColumnConstraints().addAll(column111);
			gridpane3.add(setText("ElGamal Decryption"), 0, 0);
			gridpane3.add(setProperty("Enter the ciphered message"), 0, 1);
			gridpane3.add(cipher, 0, 2);
			gridpane3.add(setProperty("Enter your secret key"), 0, 3);
			gridpane3.add(secretKey1, 0, 4);
			gridpane3.add(setProperty("Enter value of the prime number q"), 0, 5);
			gridpane3.add(order2, 0, 6);
			gridpane3.add(decryptButton, 0, 7);
			gridpane3.add(clearMessage, 0, 8);
			decrypt.setContent(gridpane3);
			
			decryptButton.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					LinkedList<BigInteger> cipherMessage = new LinkedList<BigInteger>();
					String[] tab = cipher.getText().substring(1, cipher.getText().length()-1).split(", ");
					
					for (String value : tab) {
						BigInteger bigValue = new BigInteger(value);
						cipherMessage.add(bigValue);
					}
					
					BigInteger secretKey = new BigInteger(secretKey1.getText());
					BigInteger p = new BigInteger(order2.getText());
					String clearText = decrypt(cipherMessage, secretKey, p);
					clearMessage.setText(clearText);
				}
			});
			
			root.setCenter(tabpane);
			
			primaryStage.setTitle("TP INFO325 (EL GAMAL)");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private Label setProperty(String property) {
		Label text = new Label(property);
		return text;
	}
	
	private Text setText(String value) {
		Text text = new Text(value);
		text.setId("title");
		return text;
	}
	
	private String[] keyPairGen(String order, String secretK, String gen) {
		BigInteger p, g, h, secretKey;
        Random sc = new SecureRandom();
        //Choose an integer secretKey randomly from {1,...,q-1\}.
        secretKey = secretK == null || secretK.trim().isEmpty() ? new BigInteger(64, sc) : new BigInteger(secretK);
        //Choosing a large prime p.
        p = order == null || order.trim().isEmpty() ? BigInteger.probablePrime(64, sc) : new BigInteger(order);
        //Choosing a generator element g from {1,...,q-1}.
        g = new BigInteger(gen);
        //Computing part of the public key y = (g^secretKey) mod p.
        h = g.modPow(secretKey, p);
        String[] tab = {p.toString(), g.toString(), secretKey.toString(), h.toString()};
        return tab;
	}
	
	private static LinkedList<BigInteger> encrypt(BigInteger p, BigInteger g, BigInteger h, String message) {
		Random sc = new SecureRandom();
        //Choose an integer y randomly from {1,...,q-1\}.
        BigInteger y = new BigInteger(64, sc);
        //Compute c1:=(g^y)mod(p).
        BigInteger c1 = g.modPow(y, p);
        
        String[] splitMessage = splitToNChar(message, 7);
        LinkedList<BigInteger> cipherMessage = new LinkedList<BigInteger>();
        for (String string : splitMessage) {
			BigInteger m = new BigInteger(string.getBytes());
			//Encode the text by computing m*((h^y)mod(p))mod(p)
			cipherMessage.add(m.multiply(h.modPow(y, p)).mod(p));
		}
        cipherMessage.addLast(c1);
        
        return cipherMessage;
	}
	
	private static String decrypt(LinkedList<BigInteger> cipherMessage, BigInteger secretKey, BigInteger p) {
		BigInteger c1 = cipherMessage.getLast();
		cipherMessage.removeLast();
		//Compute the modular inverse of (C1)^x modulo p, which is d = (C1)^-x
        BigInteger s = c1.modPow(secretKey, p);
        BigInteger d = s.modInverse(p);
    	//Obtain the plaintext
        String clearMessage = "";
        for (BigInteger cipherText : cipherMessage ) {
        	//This calculation produce the original message : m = cipherText*d modulo p
        	String clearText = new String((cipherText.multiply(d).mod(p)).toByteArray());
			clearMessage += clearText;
		}
        
        return clearMessage;
	}
	
	private static String[] splitToNChar(String text, int size) {
        ArrayList<String> parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }
}
